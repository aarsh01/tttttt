const express = require("express");
const axios = require("axios");
const cors = require("cors");

const app = express();
const port = 3001;

//use cors to allow cross origin resource sharing
app.use(
  cors({
    origin: `http://localhost:${port}`,
    credentials: true,
  })
);

app.get("/", (req, res) => res.send("Nothing to see here!"));

app.get("/topn/:n", async (req, res, next) => {
  console.log("'/topn' call");
  axios
  .get("https://terriblytinytales.com/test.txt", { responseType: "text" })
  .then((response) => {
    let textArr = response.data.split(/(\s+)/).filter((e) => e.trim().length > 0);
    let n = req.params.n;

      // Algo to get top n words in textArr

      let freq = {}

      for (let i = 0, len = textArr.length; i < len; i++) {
        freq[textArr[i]] = freq[textArr[i]] + 1 || 1;
      }

      let sortedByFreq = Object.keys(freq).sort((a, b) => {
        let n = freq[b] - freq[a];
        if (n !== 0) {
          return n;
        }

        return a.length > b.length ? 1 : -1;
      });

      sortedByFreq = sortedByFreq.slice(0, n);
      let resObject = [];

      for (let i = 0, len = sortedByFreq.length; i < len; i++) {
        console.log(sortedByFreq[i], freq[sortedByFreq[i]]);
        resObject.push({word: sortedByFreq[i], times: freq[sortedByFreq[i]]});
      }

      // ends

      res.json({ n: n, topn: resObject, text: response.data });
    })
    .catch((err) => next(err));
});

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
