# Top N most Occuring Words

## To Run

```cd``` into server folder

```node app.js```

```cd``` into client folder

```yarn start```

Go to ```http://localhost:3000/```.

## FrontEnd

- ReactJS
- Material UI

## BackEnd

- NodeJS
- Express
- Axios

## Details

- Client side is hosted on port 3000.
- Server side is hosted on port 3001.
- In client side TopN.js contains the frontend component.
  - It has a form which takes an integer input and calls the api in the backend which provides the required information.
  - It calls the api using axios and prints the data recieved.
- On server side, route ```http://localhost:3001/topn/:n``` is the api for getting top ```n``` numbers.
  - The api take number n as param and calculated top n most occuring words.

## Demonstration

![Alt Text](Vid/screenRec.gif)

If gif is not clear have a look at the video by clicking on this [link](https://drive.google.com/file/d/1je__iuIANyfnnOY5B7YjFrdyVt9gi-ea/view?usp=sharing)

### Things I forgot to mention in application

I forgot to mention duration. I will be available for 6 month internship. I do have college during this time but most of it will be online so this should not cause much of an issue.