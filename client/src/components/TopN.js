import React, { Component } from "react";
import axios from "axios";
import "./TopN.css";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class TopN extends Component {
  constructor() {
    super();

    this.state = {
      n: "",
      data: {},
      show: false,
    };
  }

  inputChange = (e) => {
    this.setState({ n: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (isNaN(this.state.n)) {
      alert("Must Input Number");
      return false;
    }

    axios
      .get(`/topn/${this.state.n}`)
      .then((response) => {
        this.setState({ data: response.data, show: true });
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <Container>
        <h1>
          Get Top N most Occuring Word(s) on{" "}
          <a href="http://terriblytinytales.com/test.txt">link</a>
        </h1>
        <form onSubmit={this.handleSubmit}>
          <TextField
            className="input"
            id="outlined-basic"
            label="Query number N"
            variant="outlined"
            onChange={this.inputChange}
            value={this.state.n}
          />
        </form>
        {this.state.show ? (
          <div className="accordian">
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className="heading">
                  Top {this.state.data.n} words
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell>Words</TableCell>
                        <TableCell>Frequency</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.state.data.topn.map((row, idx) => (
                        <TableRow key={idx}>
                          <TableCell component="th" scope="row">
                            {row.word}
                          </TableCell>
                          <TableCell component="th" scope="row">
                            {row.times}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </AccordionDetails>
            </Accordion>
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header"
              >
                <Typography className="heading">Whole Text</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography style={{ whiteSpace: "pre-line" }}>
                  {this.state.data.text}
                </Typography>
              </AccordionDetails>
            </Accordion>
          </div>
        ) : null}
      </Container>
    );
  }
}

export default TopN;
